
package Controller;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Model.ConexionBD;

public class Producto {

    private int id;
    private String nombre;
    private double temperatura;
    private double valorbase;

    public Producto() {
    }

    public Producto(String nombre, double temperatura, double valorbase) {
        this.nombre = nombre;
        this.temperatura = temperatura;
        this.valorbase = valorbase;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getValorbase() {
        return valorbase;
    }

    public void setValorbase(double valorbase) {
        this.valorbase = valorbase;
    }
    
    @Override
    public String toString() {
        return "\nProducto"+"\nNombre="+nombre+"\nId="+id +"\nTemperatura="+temperatura+"°C"+"\nValorBase=$"+valorbase+"\nCosto=$"+calcularCostoAlmacenamiento(valorbase);
    }

    public List<Producto> listarProductos() {
        List<Producto> listaProductos = new ArrayList<>();
        ConexionBD conexion = new ConexionBD();
        String sql = "SELECT * FROM productos;";
        try {
            ResultSet rs = conexion.consultarBD(sql);
            Producto p;
            while (rs.next()) {
                p = new Producto();
                p.setId(rs.getInt("id"));
                p.setNombre(rs.getString("Nombre"));
                p.setTemperatura(rs.getDouble("Temperatura"));
                p.setValorbase(rs.getDouble("ValorBase"));
                p.calcularCostoAlmacenamiento(rs.getDouble("Costo"));
                listaProductos.add(p);
            }
        } catch (SQLException ex) {
            System.out.println("Error al listar productos:" + ex.getMessage());
        } finally {
            conexion.cerrarConexion();
        }
        return listaProductos;
    }

    public boolean guardarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "INSERT INTO productos(Nombre,Temperatura,ValorBase,Costo)"
                + "VALUES('" + this.nombre + "'," + this.temperatura + ",'" + this.valorbase + "'," + this.calcularCostoAlmacenamiento(valorbase)+ ");";
        if (conexion.setAutoCommitBD(false)) {//Para que la bd no confirme automaticamente el cambio
            if (conexion.insertarBD(sql)) {
                conexion.commitBD();//confirma el cambio a la BD
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean actualizarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "UPDATE productos SET Nombre='"
                + this.nombre + "',Temperatura=" + this.temperatura
                + ",ValorBase='" + this.valorbase + "',Costo="
                + this.calcularCostoAlmacenamiento(valorbase) + " WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }

    public boolean eliminarProducto() {
        ConexionBD conexion = new ConexionBD();
        String sql = "DELETE FROM productos WHERE id=" + this.id + ";";
        if (conexion.setAutoCommitBD(false)) {
            if (conexion.actualizarBD(sql)) {
                conexion.commitBD();
                conexion.cerrarConexion();
                return true;
            } else {
                conexion.rollbackBD();
                conexion.cerrarConexion();
                return false;
            }
        } else {
            conexion.cerrarConexion();
            return false;
        }
    }
   
        public double calcularCostoAlmacenamiento(double aDouble) {
        if(temperatura > 21){
        return Math.round(valorbase * 1.1);
        }else{
        return Math.round(valorbase * 1.2); 
        }
    }
      
}

